function gitlab-init -a name
    if not git-is-repo
        git init -q
    end
    git remote add origin https://gitlab.com/paulsevere/$name.git
end