function ver
    set curr (realpath $argv)
    while test ! $curr = "/"
        if test -e "$curr/package.json"
            set vers (jq -r ".version" "$curr/package.json")
            set pkgname (jq -r ".name" "$curr/package.json" )
            echo $pkgname@$vers
            break
        end
        set curr (string split "/" -m 1 -r $curr)[1]
    end
end
